var VolumeSlider = {
    /**
     * Initialise the volume slider
     */
    initialise: function(startingVolume) {
        // Events for volume slider
        VolumeSlider.events.initialise();

        // Starting value
        if (typeof startingVolume == 'undefined' || startingVolume > 1 || startingVolume < 0) {
            startingVolume = 0.5;
        }
        VolumeSlider.setVolume(startingVolume);
    },

    /**
     * Set volume with value between 0 and 1
     *
     * @param value
     */
    setVolume: function(value) {
        $('#jsVolumeButtonPopoutBgSelectedVolume').height($('#jsVolumeButtonPopoutBg').height() * value);
        $('#jsVolumeButtonSRText').text('Volume (' + Math.round(value * 100) + '%)');

        VideoPlayerInterface.actions.volumeChange(value * 100);

        // Set volume logo bars
        if (value > 0.6) {
            // 2 bars
            VolumeSlider.setVolumeIconBars(2);
        } else if (value > 0.05) {
            // 1 bar
            VolumeSlider.setVolumeIconBars(1);
        } else {
            // no bars
            VolumeSlider.setVolumeIconBars(0);
        }
    },

    /**
     * Get the player volume between 0 and 1
     *
     * @returns {number}
     */
    getVolume: function() {
        return  $('#jsVolumeButtonPopoutBgSelectedVolume').height() / $('#jsVolumeButtonPopoutBg').height();
    },

    /**
     * Sets the volume image with either 0, 1 or 2 bars
     *
     * @param bars
     */
    setVolumeIconBars: function(bars) {
        $('#jsVolumeButtonIcon').removeClass("volume-0").removeClass("volume-1").removeClass("volume-2").addClass('volume-' + bars);
    },

    /**
     * Define the event handlers for the volume slider
     */
    events: {
        /**
         * Link up the events and the event handlers
         */
        initialise: function() {
            $('#jsVolumeButton').click(VolumeSlider.events.volumeButtonClickEventHandler);
            $('#jsVolumeButtonContainer').hover(VolumeSlider.events.volumeButtonHoverInEventHandler, VolumeSlider.events.volumeButtonHoverOutEventHandler);
            $('#jsVolumeButtonPopoutBg').click(VolumeSlider.events.volumePopoutClick);
            $('#jsVolumeButtonPopoutBgSelectedVolume').click(VolumeSlider.events.volumePopoutClick);
            $(document).mouseup(VolumeSlider.events.documentMouseup);
            $('#jsVolumePopoutBall').mousedown(VolumeSlider.events.volumePopoutBallMousedown);
            $(document).mousemove(VolumeSlider.events.documentMousemove);
        },

        /**
         * Is the user currently dragging volume slider
         *
         * @type {boolean}
         */
        isDragging: false,

        /**
         * Muted Volume
         * 
         * @type {number}
         */
        mutedVolume: 0,

        /**
         * Mute/unmute the volume
         */
        volumeButtonClickEventHandler: function(e) {
            var container = $('#jsVolumeButtonPopout');

            // Check we're clicking on the volume icon, not the slider
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                e.preventDefault();

                // if volume is more than 0 then mute it, otherwise full volume
                if (VolumeSlider.getVolume() > 0) {
                    // Store the volume before muting so we can revert back to the original value when we unmute
                    VolumeSlider.mutedVolume = VolumeSlider.getVolume();
                    VolumeSlider.setVolume(0);
                } else {
                    // Revert back to the original volume value
                    VolumeSlider.setVolume(VolumeSlider.mutedVolume);
                }
            }
        },

        /**
         * Display the volume control when the user hovers over the volume icon
         */
        volumeButtonHoverInEventHandler: function(e) {
            var button = $('#jsVolumeButton');
            var container = $('#');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if (!button.hasClass('open')) {
                    // Open volume control
                    button.addClass('open');
                }
            }
        },

        /**
         * Hide the volume control when the user moves their mouse away from the volume icon
         */
        volumeButtonHoverOutEventHandler: function(e) {
            var button = $('#jsVolumeButton');
            if (button.hasClass('open') && !VolumeSlider.events.isDragging) {
                // Close volume control
                button.removeClass('open');
            }
        },

        /**
         * Set the volume by clicking on the slider
         */
        volumePopoutClick: function(e) {
            var this_el = $('#jsVolumeButtonPopoutBg');
            var container = $('#jsVolumePopoutBall');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                var heightOfBar = this_el.height();
                var pxFromTopOfBar = e.pageY - this_el.offset().top;
                VolumeSlider.setVolume((heightOfBar - pxFromTopOfBar) / heightOfBar);
            }
        },

        /**
         * Toggle drag state if we're dragging the slider, and hide the popup if
         * releasing the slider outside the popup area
         */
        documentMouseup: function(e) {
            if (VolumeSlider.events.isDragging) {
                e.preventDefault();
                VolumeSlider.events.isDragging = false;

                // Hide the volume slider if releasing it outside the popup
                var container = $('#jsVolumeButton');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    VolumeSlider.events.outsideVolumeButtonMouseup();
                }
            }
        },

        /**
         * Hide the volume slider if releasing it outside the popup
         */
        outsideVolumeButtonMouseup: function() {
            var button = $('#jsVolumeButton');
            if (button.hasClass("open")) {
                $('#jsVolumeButton').removeClass('open');
            }
        },

        /**
         * Start dragging the volume slider ball
         */
        volumePopoutBallMousedown: function(e) {
            e.preventDefault();
            VolumeSlider.events.isDragging = true;
        },

        /**
         * If dragging volume slider, adjust volume as necessary
         */
        documentMousemove: function(e) {
            if (VolumeSlider.events.isDragging) {
                var bar = $('#jsVolumeButtonPopoutBg');
                var heightOfBar = bar.height();
                var pxFromTopOfBar = e.pageY - bar.offset().top;
                if (pxFromTopOfBar >= 0 && pxFromTopOfBar <= heightOfBar) {
                    VolumeSlider.setVolume((heightOfBar - pxFromTopOfBar) / heightOfBar);
                }
            }
        }
    }
};