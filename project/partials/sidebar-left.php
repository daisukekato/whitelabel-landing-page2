
            <div class="col-xs-2 side-button-container">
                <div class="row">
                    <button id="jsSideButton1" data-button="button1" class="side-button side-button__button1" title="Button 1"><span class="sr-only">Button 1</span></button>
                </div>

                <div class="row">
                    <button id="jsSideButton2" data-button="button2" class="side-button side-button__button2" title="Button 2"><span class="sr-only">Button 2</span></button>
                </div>

                <div class="row">
                    <button id="jsSideButton3" data-button="button3" class="side-button side-button__button3" title="Button 3"><span class="sr-only">Button 3</span></button>
                </div>

                <div class="row">
                    <button id="jsSideButton4" data-button="button4" class="side-button side-button__button4" title="Button 4"><span class="sr-only">Button 4</span></button>
                </div>

                <div class="row">
                    <button id="jsSideButton5" data-button="button5" class="side-button side-button__button5" title="Button 5"><span class="sr-only">Button 5</span></button>
                </div>
            </div>