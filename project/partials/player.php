            <div class="col-xs-8 player-column">
                <div class="player-wrapper">
                    <div style="min-width: 320px; width: 100%; max-width: calc(177.778vh - 54px); margin: 0 auto;">
                        <!-- Maintain aspect ratio of video player -->
                        <div class="player-container">
                            <iframe src="<?php echo $iframeVideoLink ?>"
                                    frameborder="0"
                                    scrolling="no"
                                    style="position: absolute; top: 0; right: 0; bottom: 0; left: 0; width: 100%; height: 100%; border: none;"
                                    id="videoPlayerIframe"
                                    allowfullscreen></iframe>

                            <div id="resumeSplash" class="resume-splash">
                                <img src="dist/images/resume_splash_button.png" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="player-text-bar">
                    &nbsp;
                </div>
            </div>